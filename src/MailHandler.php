<?php

namespace Drupal\supermailer;

use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Mail\MailManagerInterface;

/**
 * Default mail handler implementation.
 */
class MailHandler implements MailHandlerInterface {

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected LanguageManagerInterface $languageManager;

  /**
   * The mail manager.
   *
   * @var \Drupal\Core\Mail\MailManagerInterface
   */
  protected MailManagerInterface $mailManager;

  /**
   * Constructs a new MailHandler object.
   *
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   * @param \Drupal\Core\Mail\MailManagerInterface $mail_manager
   *   The mail manager.
   */
  public function __construct(LanguageManagerInterface $language_manager, MailManagerInterface $mail_manager) {
    $this->languageManager = $language_manager;
    $this->mailManager = $mail_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function sendMail(string $to, string $subject, array $body, array $params = []): bool {
    if (empty($to)) {
      return FALSE;
    }

    $default_params = [
      'headers' => [
        'Content-Type' => 'text/html; charset=UTF-8;',
        'Content-Transfer-Encoding' => '8Bit',
      ],
      'id' => 'mail',
      'reply-to' => NULL,
      'subject' => $subject,
      'langcode' => $this->languageManager->getCurrentLanguage()->getId(),
      // The body will be rendered in supermailer_mail(), because that's what
      // MailManager expects. The correct theme and render context aren't
      // setup until then.
      'body' => $body,
    ];
    if (!empty($params['cc'])) {
      $default_params['headers']['Cc'] = $params['cc'];
    }
    if (!empty($params['bcc'])) {
      $default_params['headers']['Bcc'] = $params['bcc'];
    }
    $params = array_replace($default_params, $params);

    $message = $this->mailManager->mail('supermailer', $params['id'], $to, $params['langcode'], $params, $params['reply-to']);

    return (bool) $message['result'];
  }

  /**
   * {@inheritdoc}
   */
  public function sendControlMail(string $subject, string $mail, string $ip, string $to): bool {
    $mail_id = 'supermail_control_' . $subject;
    $date = date("Y-m-d H:i:s");
    $body = [];
    $body[] = 'EMail: ' . $mail;
    $body[] = 'IP: ' . $ip;
    $body[] = 'DateTime: ' . $date;
    $body[] = 'DatumZeit: ' . $date;
    $params = [
      'headers' => [
        'Content-Type' => 'text/plain; charset=UTF-8;',
        'Content-Transfer-Encoding' => '8Bit',
      ],
      'subject' => $subject,
      'content_type' => 'text/plain',
      'body' => $body,
    ];
    $message = $this->mailManager->mail('supermailer', $mail_id, $to, $this->languageManager->getCurrentLanguage()->getId(), $params);
    return (bool) $message['result'];
  }

}
