<?php

namespace Drupal\supermailer\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\supermailer\CryptKeyInterface;
use Drupal\supermailer\MailHandlerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Defines the Supermailer subscription controller.
 */
class SubscriptionController extends ControllerBase {

  /**
   * The crypt key service.
   *
   * @var \Drupal\supermailer\CryptKeyInterface
   */
  protected CryptKeyInterface $cryptKey;

  /**
   * The mail handler.
   *
   * @var \Drupal\supermailer\MailHandlerInterface
   */
  protected MailHandlerInterface $mailHandler;

  /**
   * Constructs a SubscriptionController object.
   *
   * @param \Drupal\supermailer\CryptKeyInterface $crypt_key
   *   The crypt key service.
   * @param \Drupal\supermailer\MailHandlerInterface $mail_handler
   *   The mail handler.
   */
  public function __construct(CryptKeyInterface $crypt_key, MailHandlerInterface $mail_handler) {
    $this->cryptKey = $crypt_key;
    $this->mailHandler = $mail_handler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('supermailer.crypt_key'),
      $container->get('supermailer.mail_handler')
    );
  }

  /**
   * Returns the subscription confirmation page.
   *
   * @param string $mail
   *   The e-mail address.
   * @param string $hash
   *   The crypt key hash.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The current request.
   *
   * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
   *   The response.
   */
  public function confirmSubscribe(string $mail, string $hash, Request $request) {
    $config = $this->config('supermailer.settings');
    $valid_hash = $this->cryptKey->validateHash($mail, $hash);
    $recipient = $config->get('recipient');
    $success = $valid_hash && !empty($recipient);
    if ($success) {
      $ip = $request->getClientIp();
      $success = $this->mailHandler->sendControlMail('subscribe', $mail, $ip, $recipient);
    }

    if ($success) {
      $this->cryptKey->remove($mail);
      $subscribe_ok_page = $config->get('subscribe_ok_page');
      if ($subscribe_ok_page) {
        return new RedirectResponse($subscribe_ok_page);
      }
      return ['#markup' => $this->t('Thank you for signing up to our newsletter!')];
    }
    else {
      return ['#markup' => $this->t('The confirmation link is invalid. Please retry newsletter signup!')];
    }
  }

}
