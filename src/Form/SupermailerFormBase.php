<?php

namespace Drupal\supermailer\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\supermailer\MailHandlerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines the base class for subscribe and unsubscribe forms.
 */
abstract class SupermailerFormBase extends FormBase {

  /**
   * The mail handler.
   *
   * @var \Drupal\supermailer\MailHandlerInterface
   */
  protected $mailHandler;

  /**
   * Constructs a new SupermailerFormBase object.
   *
   * @param \Drupal\supermailer\MailHandlerInterface $mail_handler
   *   The mail handler.
   */
  public function __construct(MailHandlerInterface $mail_handler) {
    $this->mailHandler = $mail_handler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('supermailer.mail_handler')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['mail'] = [
      '#type' => 'email',
      '#title' => $this->t('Your email address'),
      '#required' => TRUE,
    ];

    $form['actions'] = ['#type' => 'actions'];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Send'),
    ];
	$form['#cache']['contexts'][] = 'session';
	$form['#cache']['max-age'] = 0;

    return $form;
  }

}
