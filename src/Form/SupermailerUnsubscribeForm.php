<?php

namespace Drupal\supermailer\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\supermailer\MailHandlerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Provides the Supermailer unsubscribe form.
 */
class SupermailerUnsubscribeForm extends SupermailerFormBase {

  /**
   * The current request.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $request;

  /**
   * Constructs a new SupermailerUnsubscribeForm object.
   *
   * @param \Drupal\supermailer\MailHandlerInterface $mail_handler
   *   The mail handler.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack
   */
  public function __construct(MailHandlerInterface $mail_handler, RequestStack $request_stack) {
    parent::__construct($mail_handler);

    $this->request = $request_stack->getCurrentRequest();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('supermailer.mail_handler'),
      $container->get('request_stack')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'supermailer_unsubscribe_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);

    if ($mail = $this->request->query->get('mail')) {
      $form['mail']['#default_value'] = $mail;
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $mail = $form_state->getValue('mail');
    $config = $this->config('supermailer.settings');
    $recipient = $config->get('recipient');
    $ip = $this->request->getClientIp();
    $success = $this->mailHandler->sendControlMail('unsubscribe', $mail, $ip, $recipient);

    if ($success) {
      $unsubscribe_ok_page = $config->get('unsubscribe_ok_page');
      if ($unsubscribe_ok_page) {
        $form_state->setRedirectUrl(Url::fromUserInput($unsubscribe_ok_page));
        return;
      }
      $this->messenger()->addStatus($this->t('We regret that you have unsubscribed from our newsletter!'));
    }
    else {
      $this->messenger()->addError($this->t('An error occurred. Please try again later or contact our support!'));
    }
  }

}
