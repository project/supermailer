<?php

namespace Drupal\supermailer\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\supermailer\CryptKeyInterface;
use Drupal\supermailer\MailHandlerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the Supermailer subscribe form.
 */
class SupermailerSubscribeForm extends SupermailerFormBase {

  /**
   * The crypt key service.
   *
   * @var \Drupal\supermailer\CryptKeyInterface
   */
  protected $cryptKey;

  /**
   * Constructs a new SupermailerSubscribeForm object.
   *
   * @param \Drupal\supermailer\MailHandlerInterface $mail_handler
   *   The mail handler.
   * @param \Drupal\supermailer\CryptKeyInterface $crypt_key
   *   The crypt key service.
   */
  public function __construct(MailHandlerInterface $mail_handler, CryptKeyInterface $crypt_key) {
    parent::__construct($mail_handler);

    $this->cryptKey = $crypt_key;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('supermailer.mail_handler'),
      $container->get('supermailer.crypt_key')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'supermailer_subscribe_form';
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $mail = $form_state->getValue('mail');
    $config = $this->config('supermailer.settings');
    $hash = $this->cryptKey->addOrUpdateKey($mail);
    $subject = $config->get('confirmation_mail_subject');
    $url = Url::fromRoute('supermailer.subscribe.confirm',
      ['mail' => $mail, 'hash' => $hash], ['absolute' => TRUE]);
    $body = [
      '#theme' => 'supermailer_confirmation_mail',
      '#confirmation_url' => $url->toString(),
    ];
    $params = [
      'id' => 'supermailer_confirmation_mail',
      'mail' => $mail,
      'hash' => $hash,
    ];
    $success = $this->mailHandler->sendMail($mail, $subject, $body, $params);
    if ($success) {
      $this->messenger()->addStatus($this->t('To complete the registration, please click on the confirmation link in the email we just sent you.'));
    }
    else {
      $this->messenger()->addError($this->t('Unfortunately, an error occurred while sending the confirmation email. Please try again later!'));
    }
  }

}
