<?php

namespace Drupal\supermailer;

/**
 * Handles the assembly and dispatch of HTML emails.
 *
 * Allows a render array (with an associated #theme) to be used as the
 * message body.
 *
 * Since Drupal core doesn't support HTML emails out of the box, Supermailer
 * assumes that Swiftmailer (or an appropriate alternative) is used.
 */
interface MailHandlerInterface {

  /**
   * Sends an email.
   *
   * @param string $to
   *   The address the email will be sent to. Must comply with RFC 2822.
   * @param string $subject
   *   The subject. Must not contain any newline characters.
   * @param array $body
   *   A render array representing the message body.
   * @param array $params
   *   Email parameters. Recognized keys:
   *     - id: A unique identifier of the email type.
   *       Allows hook_mail_alter() implementations to identify specific emails.
   *       Defaults to "mail". Automatically prefixed with "supermailer_".
   *     - from: The address the email will be marked as being from.
   *       Defaults to the current store email.
   *     - reply-to: The address to which the reply will be sent. No default.
   *     - cc: The CC address or addresses (separated by a comma). No default.
   *     - bcc: The BCC address or addresses (separated by a comma). No default.
   *
   * @return bool
   *   TRUE if the email was sent successfully, FALSE otherwise.
   */
  public function sendMail(string $to, string $subject, array $body, array $params = []): bool;

  /**
   * Sends an internal control mail.
   *
   * @param string $subject
   *   The subject (subscribe, unsubscribe, ...).
   * @param string $mail
   *   The subscriber's e-mail address.
   * @param string $ip
   *   The subscriber's IP address.
   * @param string $to
   *   The internal recipient address to send the mail to.
   *
   * @return bool
   *   TRUE if the email was sent successfully, FALSE otherwise.
   */
  public function sendControlMail(string $subject, string $mail, string $ip, string $to): bool;

}
