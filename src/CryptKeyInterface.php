<?php

namespace Drupal\supermailer;

/**
 * Defines the crypt key service interface.
 */
interface CryptKeyInterface {

  /**
   * Adds or updates an existing crypt key for the given mail address.
   *
   * The generated crypt key hash is returned.
   *
   * @param string $mail
   *   The e-mail address.
   *
   * @return string
   *   The generated crypt key hash to be used in confirmation link.
   */
  public function addOrUpdateKey(string $mail): string;

  /**
   * Validates the given e-mail address and crypt key hash.
   *
   * @param string $mail
   *   The e-mail address.
   * @param string $hash
   *   The crypt key hash.
   *
   * @return bool
   *   If the given e-mail address and hash pair is a valid Supermailer crypt
   *   key entry, FALSE otherwise.
   */
  public function validateHash(string $mail, string $hash): bool;

  /**
   * Removes the given e-mail address from the crypt key table.
   *
   * @param string $mail
   *   The e-mail address.
   */
  public function remove(string $mail);

  /**
   * Removes expired keys from the database.
   */
  public function cleanupExpiredKeys();

}
