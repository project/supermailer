<?php

namespace Drupal\supermailer;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Component\Utility\Crypt;
use Drupal\Core\Config\Config;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\PrivateKey;
use Drupal\Core\Site\Settings;

/**
 * Default crypt key service implementation.
 */
class CryptKey implements CryptKeyInterface {

  /**
   * The module config.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected Config $config;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected Connection $database;

  /**
   * The Drupal private key.
   *
   * @var \Drupal\Core\PrivateKey
   */
  protected PrivateKey $privateKey;

  /**
   * The time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected TimeInterface $time;

  /**
   * Constructs a new CryptKey object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection
   * @param \Drupal\Core\PrivateKey $private_key
   *   The Drupal private key.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   */
  public function __construct(ConfigFactoryInterface $config_factory, Connection $database, PrivateKey $private_key, TimeInterface $time) {
    $this->config = $config_factory->get('supermailer.settings');
    $this->database = $database;
    $this->privateKey = $private_key;
    $this->time = $time;
  }

  /**
   * {@inheritdoc}
   */
  public function addOrUpdateKey(string $mail): string {
    $now = $this->time->getRequestTime();
    $value = sprintf('%s:%s', $mail, $now);
    $hash = Crypt::hmacBase64($value, $this->privateKey->get() . $this->getHashSalt());
    $this->database->merge('supermailer_crypt_key')
      ->key('mail', $mail)
      ->fields([
        'hash' => $hash,
        'created' => $now,
      ])
      ->execute();
    return $hash;
  }

  /**
   * {@inheritdoc}
   */
  public function validateHash(string $mail, string $hash): bool {
    $threshold = $this->time->getCurrentTime() - $this->getExpirationIntervalSeconds();
    $result = $this->database->select('supermailer_crypt_key', 'sck')
      ->condition('sck.mail', $mail)
      ->condition('sck.hash', $hash)
      ->condition('sck.created', $threshold, '>=')
      ->fields('sck')
      ->countQuery()
      ->execute();
    return (int) $result->fetchField() > 0;
  }

  /**
   * {@inheritdoc}
   */
  public function remove(string $mail) {
    $this->database->delete('supermailer_crypt_key')
      ->condition('mail', $mail)
      ->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function cleanupExpiredKeys() {
    $threshold = $this->time->getCurrentTime() - $this->getExpirationIntervalSeconds();
    $this->database->delete('supermailer_crypt_key')
      ->condition('created', $threshold, '<')
      ->execute();
  }

  /**
   * Gets a salt useful for hardening against SQL injection.
   *
   * @return string
   *   A salt based on information in settings.php, not in the database.
   *
   * @throws \RuntimeException
   */
  protected function getHashSalt(): string {
    return Settings::getHashSalt();
  }

  /**
   * Get the expiration interval in seconds.
   *
   * @return int
   *   The expiration interval in seconds.
   */
  protected function getExpirationIntervalSeconds(): int {
    $expiration_interval = (int) $this->config->get('crypt_key_expires_interval');
    return $expiration_interval * 60 * 60 * 24;
  }

}
